import java.util.ArrayList;

public class Raumschiff {
	
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	
	public Raumschiff() {
	
	}


	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schileInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schileInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidAnzahl = androidAnzahl;
		this.schiffsname = schiffsname;
	}
	
	public void getPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}


	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}


	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}


	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}


	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}


	public int getSchildeInProzent() {
		return schildeInProzent;
	}


	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}


	public int getHuelleInProzent() {
		return huelleInProzent;
	}


	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}


	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}


	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}


	public int getAndroidAnzahl() {
		return androidAnzahl;
	}


	public void setAndroidAnzahl(int androidAnzahl) {
		this.androidAnzahl = androidAnzahl;
	}


	public String getSchiffsname() {
		return schiffsname;
	}


	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}


	
	public String toString() {
		return "Raumschiff [photonentorpedoAnzahl=" + photonentorpedoAnzahl + ", energieversorgungInProzent="
				+ energieversorgungInProzent + ", schildeInProzent=" + schildeInProzent + ", huelleInProzent="
				+ huelleInProzent + ", lebenserhaltungssystemeInProzent=" + lebenserhaltungssystemeInProzent
				+ ", androidAnzahl=" + androidAnzahl + ", schiffsname=" + schiffsname + "]";
	}
	
	public void zustandRaumschiff() {
		System.out.println("Raumschiff photonentorpedoAnzahl = " + photonentorpedoAnzahl + " \nenergieversorgungInProzent = "
				+ energieversorgungInProzent + " \nschildeInProzent = " + schildeInProzent + " \nhuelleInProzent = "
				+ huelleInProzent + " \nlebenserhaltungssystemeInProzent = " + lebenserhaltungssystemeInProzent
				+ " androidAnzahl = " + androidAnzahl + " \nschiffsname = " + schiffsname + " \n");
		
		
	}
	
	public void ladungsverzeichnis () {
		for (int i = 0; i < this.ladungsverzeichnis.size(); i++)
			System.out.println(" Ladungsname: " + this.ladungsverzeichnis.get(i).getBezeichnung() + "Ladungsmenge: " + this.ladungsverzeichnis.get(i).getMenge());
		
		
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) {
		if (this.photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("\n-=*Click*=-");
		} else {
			this.photonentorpedoAnzahl -= 1;
			nachrichtAnAlle("\nPhotonentorpedo abgeschossen");
			treffer(r); 
		}
		
	}
		public void phaserkanonoSchiessen(Raumschiff r) {
			if (this.getEnergieversorgungInProzent() <= 50) {
				nachrichtAnAlle("\n-=*Click*=-");
			} else {
				this.energieversorgungInProzent -= 50;
				nachrichtAnAlle("\nPhaserkanono abgeschossen");
				treffer(r); 
			}
 
	}


	public void treffer(Raumschiff r) {
		System.out.println();
		if (this.getHuelleInProzent() <= 100) {
			nachrichtAnAlle("[Schiffsname] wurde getroffen!");
		}
		
	}


	public void nachrichtAnAlle(String nachricht) {
		System.out.println(nachricht);
		broadcastKommunikator.add(nachricht);
		
	}
	
	public void trefferVermerken(Raumschiff r) {
		System.out.println(r.schiffsname + " wurde getroffen!");
		r.schildeInProzent = (int) (r.schildeInProzent * 0.5);
		if (r.schildeInProzent == 0 ) {
			r.energieversorgungInProzent = (int) (r.energieversorgungInProzent * 0.5);
			r.huelleInProzent = (int) (r.huelleInProzent * 0.5);
		}
		if(r.huelleInProzent == 0) {
			r.lebenserhaltungssystemeInProzent = 0;
			r.nachrichtAnAlle("Die Lebenserhaltungssystem wurden vernichtet.");
		}
	}
	
	
}
