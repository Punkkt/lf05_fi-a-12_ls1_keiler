
public class RauschiffTest {

	public static void main(String[] args) {
		
		
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100,2, "IKS Hegh'ta");
//		System.out.println(klingonen);
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100,2, "IRW Khazara");
//		System.out.println(romulaner);
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
//		System.out.println(vulkanier);
		
		
		Ladung A1 = new Ladung("Bat'leth klingonen Schwert", 200);
		klingonen.addLadung(A1);
		Ladung A2 = new Ladung("Ferengi Schneckensaft", 200);
		klingonen.addLadung(A2);
		Ladung B1 = new Ladung("Borg-Schrott", 5);
		romulaner.addLadung(B1);
		Ladung B2 = new Ladung("Rote Materie", 2);
		romulaner.addLadung(B2);
		Ladung B3 = new Ladung("Plasma-Waffe", 50);
		romulaner.addLadung(B3);
		Ladung C1 =new Ladung("Forschungssonde", 35);
		vulkanier.addLadung(C1);
		Ladung C2 = new Ladung("Photonentorpedo", 3);
		vulkanier.addLadung(C2);
		
		
		
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnis();
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnis();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnis();
		
		klingonen.photonentorpedoSchiessen(klingonen);
		klingonen.phaserkanonoSchiessen(klingonen);
		
		klingonen.zustandRaumschiff();
		
		klingonen.treffer(vulkanier);
		klingonen.nachrichtAnAlle(null);
		

		
		
		
//		System.out.println(A1);
//		System.out.println(A2);
		
		
	}

}
