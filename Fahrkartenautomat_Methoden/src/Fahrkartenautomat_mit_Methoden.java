
import java.util.Scanner;
class Fahrkartenautomat_mit_Methoden
{
    public static void main(String[] args)
    {
       
    	while(true) {
    	
    	Scanner tastatur = new Scanner(System.in);
      
       /* int gew�hlt, da es hier keinen Unterschied macht, ob man byte, short oder int benutzt und int
        * die g�ngigste Methode ist.
        */ 
       
       int tickets;
       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgabe();
       rueckgeldAusgeben(eingezahlterGesamtbetrag);
    	
    	
    	} 
    	
    }
    

   //
   // Erste Methode 
    
    
    public static double fahrkartenbestellungErfassen() {
        Scanner erfassungsScan = new Scanner(System.in);
        int ticketChoice = 0; 
        double ticketPreis = 0; 
        double zuZahlenderBetrag = 0; 
        while (ticketChoice != 9) {
	        System.out.println("Fahrkartenbestellvorgang:\r\n"
	    			+ "=========================\r\n"
	    			+ "\r\n"
	    			+ "W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\r\n"
	    			+ "  Einzelfahrschein Regeltarif AB [3,00 EUR] (1)\r\n"
	    			+ "  Tageskarte Regeltarif AB [8,80 EUR] (2)\r\n"
	    			+ "  Kleingruppen-Tageskarte Regeltarif AB [25,50 EUR] (3)"
	    			+ "  Bezahlen (9)\n\n"
	    			+ "Ihre Wahl:");
	        
	        ticketChoice = erfassungsScan.nextInt(); 
	        if ((ticketChoice < 1 || ticketChoice > 3) && ticketChoice != 9) {
	        	System.out.println("Bitte verwenden Sie die vorgegebenen Zahlen, vielen Dank!");
	        	continue; 
	        } else if (ticketChoice == 1) {
	        	ticketPreis += 3; 
	        } else if (ticketChoice == 2) {
	        	ticketPreis += 8.80;
	        } else if (ticketChoice == 3) {
	        	ticketPreis += 25.50; 
	        } else if (ticketChoice == 9) {
	        	break; 
	        }
	        
	        
	        
	        System.out.println("Zwischensumme: " + ticketPreis + " � ");
	        
        System.out.println("Anzahl der Tickets: ");
        int ticketAnzahl = erfassungsScan.nextInt();
        while (ticketAnzahl <= 0 || ticketAnzahl > 10) {
        	System.out.println(" >> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
        	System.out.println("Anzahl der Tickets: ");
        	ticketAnzahl = erfassungsScan.nextInt();
        }	
        
        zuZahlenderBetrag = ticketPreis * (double)ticketAnzahl; 
      
        }
        return zuZahlenderBetrag; 
    }

    // Zweite Methode fahrkartenBezahlen
    
    public static double fahrkartenBezahlen (double zuZahlen) {
    	 
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
         while(eingezahlterGesamtbetrag < zuZahlen) {
        	 
      	   System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlen - eingezahlterGesamtbetrag));
      	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
      	  double  eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
         }
         eingezahlterGesamtbetrag -= zuZahlen; 
    	
    	return (eingezahlterGesamtbetrag);

    	
    }
   
    
    
   //Dritte Methode  fahrkartenAusgeben
    
    
   public static void fahrkartenAusgabe () {
	   System.out.println("\nFahrscheine werden ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       
  }
    
    // Methode 4
   
   public static void rueckgeldAusgeben (double r�ckgeld) {
	   
	   
       double r�ckgabebetrag = r�ckgeld ;
       
       	if(r�ckgabebetrag > 0.0)
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro \n", r�ckgabebetrag);
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
                          "vor jeweiligen Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       
   
       
   
   }
}
   
   
	   
	   
	   